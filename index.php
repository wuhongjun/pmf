<?php
/* --- 若无特别需求,请勿修改此文件! --- */
// define('_PAGE_START_MEM_', memory_get_usage(true)); // 页面开始时内存情况
define('_PAGE_START_TIME_', microtime(true)); // 页面开始时间,也可作“time()”用
define('IN_PMF', true); // 强制单一入口
$strTemp = str_replace('\\', '/', dirname(__FILE__));
include($strTemp . '/configs.inc.php');
include($strTemp . '/globalFunctions.inc.php');
$_m_ = '';
$_c_ = 'index';
$_a_ = 'index';
$strTemp = '';
$blnTemp = true;
if (CLI_MODE) { // 命令行模式
    if ($argc == 2) {
        $_a_ = $argv[1];
    } elseif ($argc == 3) {
        $_c_ = $argv[1];
        $_a_ = $argv[2];
    } elseif ($argc > 3) {
        $_m_ = $argv[1];
        $_c_ = $argv[2];
        $_a_ = $argv[3];
    }
} elseif (USE_PATH_INFO && isset($_SERVER['PATH_INFO']) && !empty($_SERVER['PATH_INFO'])) { // 分析静态路由: http://xxx/index.php/m/c/a.html
    $strTemp = $_SERVER['PATH_INFO'];
    ('' != URL_EXT) && ($strTemp = str_replace(URL_EXT, '', $strTemp));
    $arrTemp = explode('/', substr($strTemp, 1));
    $intTemp = count($arrTemp);
    if ($intTemp == 1) {
        (!empty($arrTemp[0])) && ($_a_ = $arrTemp[0]);
    } elseif ($intTemp == 2) {
        (!empty($arrTemp[0])) && ($_c_ = $arrTemp[0]);
        (!empty($arrTemp[1])) && ($_a_ = $arrTemp[1]);
    } elseif ($intTemp >= 3) {
        (!empty($arrTemp[0])) && ($_m_ = $arrTemp[0]);
        (!empty($arrTemp[1])) && ($_c_ = $arrTemp[1]);
        (!empty($arrTemp[2])) && ($_a_ = $arrTemp[2]);
    }
} else { // 分析动态路由: http://xxx/index.php?m=???&c=???&a=???
    isset($_GET['c']) && (!empty($_GET['c'])) && ($_c_ = $_GET['c']);
    isset($_GET['a']) && (!empty($_GET['a'])) && ($_a_ = $_GET['a']);
    isset($_GET['m']) && (!empty($_GET['m'])) && ($_m_ = $_GET['m']);
}
define('_ROUTE_M_', $_m_);
define('_ROUTE_C_', $_c_);
define('_ROUTE_A_', $_a_);
$_c_ = ucfirst($_c_);
if (!empty($_m_)) { // 控制器在文件夹中?
    $strTemp = CTRLS_PATH . '/' . $_m_ . '/Ctrl' . $_c_ . '.class.php';
    file_exists($strTemp) && is_file($strTemp) && require($strTemp);
    $blnTemp = false;
}
$strTemp = 'Ctrl' . $_c_;
if (class_exists($strTemp, $blnTemp)) {
    $objTemp = new $strTemp();
    $strTemp = '';
    (!is_callable(array($objTemp, $_a_))) && (list($strTemp, $_a_) = array($_a_, '_remap')); // 调用_remap方法
    call_user_func(array($objTemp, $_a_), $strTemp);
} else {
    jsLoadTo('/', '页面不存在!');
}