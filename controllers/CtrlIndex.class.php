<?php
/* ----- 非法请求 ----- */
defined('IN_PMF') || die('403 Forbidden !!!');

/**
 * 默认控制器，此文件请勿删除
 */
class CtrlIndex extends Controller {
    
    /**
     * 网站首页
     */
    public function index() {
        echo 'haha';
    }
    
    /**
     * 测试页面静态缓存
     */
    public function cache() {
        //$this->_blnPrint = false;
        /* 缓存5秒，如果是-1的话，表示永久 */
        $this->_cache(5) && die(' ----- This is from cache -----');
        /* 模板赋值 */
        $arrAssign = array(
            'date' => date('Y-m-d H:i:s'),
        );
        /* 显示 */
        $this->_display('index_cache.html', $arrAssign);
    }
    
    /**
     * 测试Mysqli连接数据库
     */
    public function mysql() {
        $db = Db::getInstance(); // 单例连接
        $arr = $db->showTables();
        print_r($arr);
    }
    
    /**
     * 测试Session
     */
    public function sess() {
        sessStart(); // 相当于session_start()
        if (!isset($_SESSION['test'])) {
            $_SESSION['test'] = date('Y-m-d H:i:s');
        }
        echo $_SESSION['test'];
    }
    
    /**
     * 测试单例载入模型
     */
    public function modl() {
        $a = loadModel('test'); // 单例载入模型
        $a->val = 'haha';
        $b = loadModel('test');
        echo $b->val;
    }
    
    /**
     * 测试_remap方法
     */
    public function _remap($strAction) {
        echo $strAction;
    }
    
    /**
     * 验证码
     */
    public function chkimg() {
        $obj = new CheckImg();
        $obj->Draw();
    }
    
    /**
     * 测试cookie
     */
    public function cookie() {
        Cookie::set('test', date('Y-m-d H:i:s'), 3600);
        //var_dump(Cookie::get('test'));
        //Cookie::del('test');
    }
}