<?php
/* ----- 非法请求 ----- */
defined('IN_PMF') || die('403 Forbidden !!!');

/* ----- 这里的函数将会全局加载，若无特别需求，请勿更改 ----- */

/**
 * 类的自动加载
 */
function clsAutoLoad($strClsName) {
    $strPath = '';
    $strTemp = substr($strClsName, 0, 4);
    $strClsName = '/' . $strClsName . '.class.php';
    $blnTemp = false;
    if ('Ctrl' == $strTemp) {
        $strPath = CTRLS_PATH . $strClsName;
        $blnTemp = (file_exists($strPath) && is_file($strPath));
    }
    if (!$blnTemp && 'Modl' == $strTemp) {
        $strPath = MDLS_PATH . $strClsName;
        $blnTemp = (file_exists($strPath) && is_file($strPath));
    }
    if (!$blnTemp) {
        $strPath = CLAS_PATH . $strClsName;
        $blnTemp = (file_exists($strPath) && is_file($strPath));
    }
    if ($blnTemp) {
        require($strPath);
    } else {
        die('Can NOT find class : ' . substr($strClsName, 1));
    }
}
spl_autoload_register('clsAutoLoad');

/**
 * 载入模型
 */
function loadModel($strName, $blnInit = true) {
    static $arrSingleModels = array();
    $objRe = null;
    if (!empty($strName)) {
        $strKey = md5($strName);
        if (isset($arrSingleModels[$strKey])) {
            $objRe = $arrSingleModels[$strKey];
        } else {
            $strClass = 'Modl' . ucfirst($strName);
            if (class_exists($strClass, true)) {
                if ($blnInit) {
                    $objRe = new $strClass();
                    $arrSingleModels[$strKey] = $objRe;
                } else {
                    $objRe = true;
                }
            }
        }
    }
    return $objRe;
}

/**
 * session_start()
 */
function sessStart() {
    if (!defined('SESS_STARTED')) {
        define('SESS_STARTED', true);
        new Session(SESS_STORAGE);
    }
}

/**
 * 处理反斜杠 
 */
if (function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) {
    function stripslashes_deep($value) {
        return is_array($value) ? array_map('stripslashes_deep', $value) : stripslashes($value);
    }
    $_POST = array_map('stripslashes_deep', $_POST);
    $_GET = array_map('stripslashes_deep', $_GET);
    $_COOKIE = array_map('stripslashes_deep', $_COOKIE);
    $_REQUEST = array_map('stripslashes_deep', $_REQUEST);
}

/**
 * 模板中常用的打印短函数
 */
function p($str = null, $strDefault = null) {
    if (!(null === $str || '' === $str)) {
        print($str);
    } elseif (!(null === $strDefault || '' === $strDefault)) {
        print($strDefault);
    }
}

/**
 * 关闭窗口
 */
function jsCloseWindow($strMsg = '', $strExtJs = '') {
    $strAlert = !empty($strMsg) ? 'alert("' . addslashes($strMsg) . '");' : '';
    $strJs = <<<JS
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <script type="text/javascript">
            {$strAlert}
            {$strExtJs}
            window.close();
        </script>
    </body>
</html>
JS;
    define('DO_NOT_CACHE', true);
    die($strJs);
}

/**
 * JS跳转
 */
function jsLoadTo($strUrl = '/', $strMsg = '', $intMS = 0) {
    '/' == $strUrl && $strUrl = HTTP_URL;
    empty($strMsg) || $strMsg = 'alert("' . addslashes($strMsg) . '");';
    $strJs = <<<JS
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <script type="text/javascript">
            {$strMsg}
            setTimeout(function() {
                window.top.location.href = "{$strUrl}";
            }, {$intMS});
        </script>
    </body>
</html>
JS;
    define('DO_NOT_CACHE', true);
    die($strJs);
}

/* ----- 请把你的函数写在下面 ----- */