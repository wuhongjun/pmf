<?php
/* ----- 非法请求 ----- */
defined('IN_PMF') || die('403 Forbidden !!!');

/**
 * Cookie操作类
 */
class Cookie {
    
    /**
     * 设置
     */
    public static function set($key, $val = '', $exp = 0, $path = '/', $domain = '') {
        if (function_exists('mcrypt_module_open')) {
            $objDes = new DES();
            $val = $objDes->encode($val);
        }
        return setcookie($key, $val, intval(_PAGE_START_TIME_) + $exp, $path, $domain);
    }
    
    /**
     * 读取
     */
    public static function get($key) {
        $re = false;
        if (is_array($_COOKIE) && isset($_COOKIE[$key])) {
            $re = $_COOKIE[$key];
            if (function_exists('mcrypt_module_open')) {
                $objDes = new DES();
                $re = $objDes->decode($re);
            }
        }
        return $re;
    }
    
    /**
     * 删除
     */
    public static function del($key) {
        $re = false;
        if (is_array($_COOKIE) && isset($_COOKIE[$key])) {
            $re = setcookie($key, "", intval(_PAGE_START_TIME_) - 86400);
        }
        return $re;
    }
}