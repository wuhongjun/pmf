运行环境关键词：

    php5.3+

    apache2.2+/nginx
    
    mysql5+
    
    mysqli
    
    curl
    
    mcrypt
    
    mbstring
    
    utf8
    
    json
    
    gd
    
    urlrewrite


框架特点：
    
    1.方便的配置
    
    2.文件包含和自动加载
    
    3.单一入口
    
    4.优雅的路由地址
    
    5.MVC
    
    6.单例性能
    
    7.页面/文件静态
    
    8.session存储定制
    
    9.内置了一些有用的类
    
    10.极易扩展/二次开发