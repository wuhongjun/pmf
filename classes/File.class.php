<?php
/* ----- 非法请求 ----- */
defined('IN_PMF') || die('403 Forbidden !!!');

/**
 * 文件读写类
 */
class File {
    
    /**
     * 创建目录
     */
    public static function makeDir($strDir, $mod = 0777) {
        return is_dir($strDir) || (self::makeDir(dirname($strDir), $mod) && mkdir($strDir, $mod));
    }
    
    /**
     * 取目录中的文件(夹)
     */
    public static function getFilesInDir($strDir, $arrExt = array()) {
        $arrRe = array();
        if (is_dir($strDir) && file_exists($strDir)) {
            $hdl = opendir($strDir);
            is_array($arrExt) || $arrExt = array($arrExt);
            while (($file = readdir($hdl)) !== false) {
                if ($file != "." && $file != "..") {
                    $file = $strDir . '/' . $file;
                    if (is_dir($file)) {
                        $arrExt || array_push($arrRe, $file);
                        $arrRe = array_merge($arrRe, self::getFilesInDir($file, $arrExt));
                    } else {
                        if ($arrExt) {
                            in_array(substr($file, strrpos($file, '.')), $arrExt) && array_push($arrRe, $file);
                        } else {
                            array_push($arrRe, $file);
                        }
                    }
                }
            }
            closedir($hdl);
        }
        return $arrRe;
    }
    
    /**
     * 删除目录
     */
    public static function delDir($strDir) {
        $arrFile = self::getFilesInDir($strDir);
        foreach ($arrFile as $file) {
            if (is_dir($file)) {
                self::delDir($file);
            } elseif (is_file($file)) {
                unlink($file);
            }
        }
        return is_dir($strDir) && rmdir($strDir);
    }
    
    /**
     * 创建文件
     */
    public static function write($strFile, $strContent = '', $mod = 'w') {
        $re = false;
        self::makeDir(dirname($strFile));
        $hdl = fopen($strFile, $mod);
        if ($hdl) {
            flock($hdl, LOCK_EX);
            $re = is_writable($strFile) && fwrite($hdl, $strContent);
            flock($hdl, LOCK_UN);
            fclose($hdl);
        }
        return $re;
    }
    
    /**
     * 读取文件内容
     */
    public static function read($strFile) {
        return (file_exists($strFile) && is_file($strFile)) ? file_get_contents($strFile) : false;
    }
    
    /**
     * 按行读取文件
     */
    public static function readLines($strFile) {
        return (file_exists($strFile) && is_file($strFile)) ? file($strFile) : false;
    }
    
    /**
     * 删除文件
     */
    public static function del($strFile) {
        return (file_exists($strFile) && is_file($strFile)) ? unlink($strFile) : false;
    }
}