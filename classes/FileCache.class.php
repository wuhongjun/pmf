<?php
/* ----- 非法请求 ----- */
defined('IN_PMF') || die('403 Forbidden !!!');

/**
 * 文件缓存类
 */
class FileCache {
    private $strPath; // 缓存文件目录
    private $intMaxGc; // 回收时最多检查文件数
    private $strExt = '.html'; // 缓存文件扩展名
    
    /**
     * 构造函数
     */
    public function __construct($strPath, $intMaxGc = 10, $strExt = '.html') {
        if (!$strPath || !is_string($strPath)) {
            die('参数错误，请指定缓存目录！');
        } elseif (!is_numeric($intMaxGc) || $intMaxGc < 1) {
            die('参数错误，第二个参数必须是个大于０的正整数！');
        } else {
            $this->strPath = $strPath;
            $this->intMaxGc = $intMaxGc;
            $this->strExt = $strExt;
        }
    }
    
    /**
     * 垃圾回收
     */
    public function gc() {
        $hdl = opendir($this->strPath);
        $intCnt = 0;
        while ($intCnt < $this->intMaxGc && ($file = readdir($hdl)) !== false) {
            if ($file != "." && $file != "..") {
                $file = $this->strPath . '/' . $file;
                if (is_dir($file)) {
                    File::delDir($file);
                } elseif (is_file($file)) {
                    $arrData = File::read($file);
                    if (!is_array($arrData) || !isset($arrData['created_at']) || !isset($arrData['data']) || !isset($arrData['exp'])) {
                        File::del($file);
                    } elseif (($arrData['exp'] > 0) && (intval($arrData['created_at']) + intval($arrData['exp']) < _PAGE_START_TIME_)) {
                        File::del($file);
                    }
                }
                $intCnt ++;
            }
        }
    }
    
    /**
     * 删除全部
     */
    public function delAll() {
        return File::delDir($this->strPath);
    }
    
    /**
     * 删除缓存
     */
    public function del($strName) {
        $strName = $this->_genPath($strName);
        false === $strName && die('缓存名只能由数字/字母/下划线组成！');
        return File::del($strName);
    }
    
    /**
     * 验证缓存名
     */
    private function _checkName($strName) {
        return (is_string($strName) && !empty($strName) && preg_match("/^[a-zA-Z0-9_]+$/", $strName));
    }
    
    /**
     * 构造文件路径
     */
    private function _genPath($strName) {
        return $this->_checkName($strName) ? $this->strPath . '/' . $strName . $this->strExt : false;
    }
    
    /**
     * 写缓存文件
     */
    public function set($strName, $strData, $intExp = 300) {
        $strName = $this->_genPath($strName);
        false === $strName && die('缓存名只能由数字/字母/下划线组成！');
        $arrData = array(
            'created_at' => _PAGE_START_TIME_,
            'data' => $strData,
            'exp' => $intExp
        );
        return File::write($strName, json_encode($arrData));
    }
    
    /**
     * 读缓存文件
     */
    public function get($strName) {
        $strName = $this->_genPath($strName);
        false === $strName && die('缓存名只能由数字/字母/下划线组成！');
        $re = false;
        $arrData = File::read($strName);
        if ($arrData) {
            $arrData = @json_decode($arrData, true);
            if (is_array($arrData) && isset($arrData['created_at']) && isset($arrData['data']) && isset($arrData['exp'])) {
                if (($arrData['exp'] == -1) || (intval($arrData['created_at']) + intval($arrData['exp']) > _PAGE_START_TIME_)) {
                    $re = $arrData['data'];
                } else {
                    File::del($strName);
                }
            }
        }
        return $re;
    }
}