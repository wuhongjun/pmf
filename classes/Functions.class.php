<?php
/* ----- 非法请求 ----- */
defined('IN_PMF') || die('403 Forbidden !!!');

/**
 * 公用函数类
 */
class Functions {
    
    /* ----- 下面这些函数，按需静态调用，若无特别需求，请勿更改 ----- */
    
    /**
     * 取随机字符串
     */
    public static function getRandStr($how = 4, $only = '') {
        srand((double)microtime() * 1000000); // 初始化随机数种子
        $alpha = "abcdefghjkmnpqrstuvwxyABCDEFGHJKLMNPQRSTUVWXY"; // 验证码内容1:字母
        $number = "3456789"; // 验证码内容2:数字
        if ('number' == $only) {
            $alpha = $number;
        } elseif ('alpha' == $only) {
            $number = $alpha;
        }
        $randcode = '';
        $i = 0;
        while ($i < $how) {
            $i ++;
            $str = mt_rand(0, 1) ? $alpha : $number;
            $randcode .= substr($str, mt_rand(0, (strlen($str) - 1)), 1); // 逐位加入验证码字符串
        }
        return $randcode;
    }
    
    /**
     * 取数组的维度
     */
    public static function getArrDeep($arr) {
        $re = 0;
        if (is_array($arr)) {
            $re = 1;
            $arrTemp = array();
            foreach ($arr as $a) {
                is_array($a) && array_push($arrTemp, self::getArrDeep($a));
            }
            count($arrTemp) > 0 && $re += max($arrTemp);
        }
        return $re;
    }
    
    /**
     * 检查用户输入是否有以下字符
     */
    public static function hasBadword($str) {
        $bln = false;
        $arr = array("\\", '&', ' ', '　', "'", '"', '/', '*', ',', '<', '>', "\r", "\t", "\n", "#");
        foreach($arr as $s) {
            if (strpos($str, $s) !== false) {
                $bln = true;
                break;
            }
        }
        return $bln;
    }
    
    /**
     * UTF8字符截取
     */
    public static function strCut($string, $length, $dot = '...') {
        $strlen = strlen($string);
        if ($strlen <= $length) {
            return $string;
        }
        $string = str_replace(array(' ','&nbsp;', '&amp;', '&quot;', '&#039;', '&ldquo;', '&rdquo;', '&mdash;', '&lt;', '&gt;', '&middot;', '&hellip;'), array('∵', ' ', '&', '"', "'", '“', '”', '—', '<', '>', '·', '…'), $string);
        $strcut = '';
        $length = intval($length - strlen($dot) - $length/3);
        $n = $tn = $noc = 0;
        $strlen = strlen($string);
        while ($n < $strlen) {
            $t = ord($string[$n]);
            if($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
                $tn = 1; $n++; $noc++;
            } elseif (194 <= $t && $t <= 223) {
                $tn = 2; $n += 2; $noc += 2;
            } elseif (224 <= $t && $t <= 239) {
                $tn = 3; $n += 3; $noc += 2;
            } elseif (240 <= $t && $t <= 247) {
                $tn = 4; $n += 4; $noc += 2;
            } elseif (248 <= $t && $t <= 251) {
                $tn = 5; $n += 5; $noc += 2;
            } elseif ($t == 252 || $t == 253) {
                $tn = 6; $n += 6; $noc += 2;
            } else {
                $n++;
            }
            if ($noc >= $length) {
                break;
            }
        }
        if ($noc > $length) {
            $n -= $tn;
        }
        $strcut = substr($string, 0, $n);
        $strcut = str_replace(array('∵', '&', '"', "'", '“', '”', '—', '<', '>', '·', '…'), array(' ', '&amp;', '&quot;', '&#039;', '&ldquo;', '&rdquo;', '&mdash;', '&lt;', '&gt;', '&middot;', '&hellip;'), $strcut);
        return $strcut . $dot;
    }
    
    /**
     * 是Ajax请求吗?
     */
    public static function isAjaxRequest() {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest');
    }
    
    /**
     * 获得索引
     */
    public static function genIndexStr($str, $code = 'utf8') {
        $strRe = '';
        $str = strip_tags($str);
        $str = preg_replace("/[[:punct:]]/", ' ', $str);
        $intLen = mb_strlen($str, $code);
        if ($intLen > 0) {
            $intTemp = 0;
            $arrTemp = array();
            while ($intTemp < $intLen) {
                $strTemp = mb_substr($str, $intTemp, 1, $code);
                preg_match('/^[0-9a-zA-Z\s]$/', $strTemp, $arrTemp);
                $strRe .=  ((count($arrTemp) > 0) ? $strTemp : str_replace('%', '', urlencode($strTemp)));
                $intTemp ++;
            }
        }
        return trim($strRe);
    }
    
    /**
     * 生成路径文件
     */
    public static function getUploadFilePath($strExt = null) {
        $strPath = UPLOADS_PATH . '/' . date('Y') . '/' . date('m') . '/' . date('d');
        File::makeDir($strPath);
        return array(
            'path' => $strPath,
            'name' => !is_null($strExt) ? (date('His') . self::getRandStr()) . $strExt : ''
        );
    }
    
    /**
     * 处理单个上传的文件
     */
    public static function saveUploadedFile($strField = 'img_file', $arrAllowed = array('.jpg', '.jpeg', '.gif', '.png'), $arrNotAllowed = array('.php', '.exe', '.html'), $intMaxSize = 10485760) {
        $arrRe = array('ok' => false, 'path' => '', 'msg' => '');
        if ($_FILES && $_FILES[$strField] && $_FILES[$strField]['name']) {
            if ($_FILES[$strField]['size'] > $intMaxSize) {
                $arrRe['msg'] = 'ERR_1'; // 文件过大
            } else {
                $strExt = substr($_FILES[$strField]['name'], strrpos($_FILES[$strField]['name'], '.'));
                $strExt = strtolower($strExt);
                if (is_array($arrAllowed) && !in_array($strExt, $arrAllowed)) {
                    $arrRe['msg'] = 'ERR_2'; // 不允许此类型文件
                } elseif (is_array($arrNotAllowed) && in_array($strExt, $arrNotAllowed)) {
                    $arrRe['msg'] = 'ERR_2'; // 不允许此类型文件
                } else {
                    $arrTemp = self::getUploadFilePath($strExt);
                    $strPath = $arrTemp['path'];
                    $strFileName = $arrTemp['name'];
                    if (move_uploaded_file($_FILES[$strField]['tmp_name'], $strPath . '/' . $strFileName)) {
                        $arrRe['ok'] = true;
                        $arrRe['path'] = str_replace(ROOT_PATH, '', $strPath) . '/' . $strFileName;
                    } else {
                        $arrRe['msg'] = 'ERR_3'; // 上传最终失败
                    }
                }
            }
        } else {
            $arrRe['msg'] = 'ERR_0'; // 没有文件
        }
        return $arrRe;
    }
    
    /**
     * 取当前页面的URL
     */
    public static function getCurUrl() {
        $strRe = false;
        if (CLI_MODE) {
            global $argv;
            $strRe = implode(' ', $argv);
        } elseif (isset($_SERVER) && is_array($_SERVER)) {
            $strRe = (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') ? 'https://' : 'http://';
            $strRe .= isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
            if (isset($_SERVER['REQUEST_URI'])) {
                $strRe .= $_SERVER['REQUEST_URI'];
            } else {
                $strRe .= $_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME'];
                if (isset($_SERVER['QUERY_STRING'])) {
                    $strRe .= '?' . $_SERVER['QUERY_STRING'];
                } else {
                    $strRe .= isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';
                }
            }
        }
        return $strRe;
    }
    /* ----- 请把你的function添加到下面，记得“public static” ----- */
}