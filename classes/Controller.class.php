<?php
/* ----- 非法请求 ----- */
defined('IN_PMF') || die('403 Forbidden !!!');

/**
 * 控制器的基类,可以按需求定制
 */
abstract class Controller {
    
    private $_cache_seconds = 0; // 缓存的时间长度
    private $_cache_base_dir = 'pages';
    private $_cache_dir = ''; // 缓存目录
    private $_cache_key = ''; // 缓存KEY
    private $_blnUsedCache = false; // 取Cache输出的是吗?
    private $objFileCache = null;
    public $_blnPrint = true; // 在屏幕上显示出内容吗？
    
    /**
     * 构造方法
     */
    public function __construct() {
        CLI_MODE || ob_start();
    }
    
    /**
     * 默认的首页
     * --- 若无特别需求,请勿修改此函数,谢谢 ---
     */
    public function index() {
        echo 'Waiting for index page...';
    }
    
    /**
     * _remap() 方法
     */
    public function _remap($strAction) {
        jsLoadTo('/', '页面不存在!');
    }
    
    /**
     * 缓存设置,此处可以自定义
     */
    private function _setCache() {
        if ($this->_cache_seconds !== 0 && null === $this->objFileCache) {
            $this->objFileCache = new FileCache($this->_cache_dir);
        }
    }
    
    /**
     * 缓存
     */
    protected function _cache($intSeconds = 0, $strExt = '', $strFolder = '') {
        if ($intSeconds !== 0) {
            $this->_cache_seconds = $intSeconds;
            $this->_cache_key = md5(_ROUTE_M_ . '_' . _ROUTE_C_ . '_' . _ROUTE_A_);
            $this->_cache_dir = CACHES_PATH . '/' . $this->_cache_base_dir . '/';
            $this->_cache_dir .= empty($strFolder) ? $this->_cache_key : $strFolder;
            $this->_cache_key .= '_' . $intSeconds;
            is_string($strExt) && !empty($strExt) && $this->_cache_key .= '_' . $strExt;
            $this->_cache_key = md5($this->_cache_key);
            $this->_setCache();
            if (!is_null($this->objFileCache)) {
                $strContent = $this->objFileCache->get($this->_cache_key);
                if (false !== $strContent) {
                    $this->_blnUsedCache = true;
                    print($strContent);
                }
            }
        }
        return $this->_blnUsedCache;
    }
    
    /**
     * 析构函数
     */
    public function __destruct() {
        if ($this->_cache_seconds !== 0 && !$this->_blnUsedCache) {
            if (!defined('DO_NOT_CACHE') && null !== $this->objFileCache) {
                $this->objFileCache->set($this->_cache_key, $this->_getContents(), $this->_cache_seconds);
                rand(1, 3) == 2 && $this->objFileCache->gc();
            }
        }
        if (!CLI_MODE) {
            if ($this->_blnPrint) {
                ob_end_flush();
            } else {
                ob_end_clean();
            }
        }
    }
    
    /**
     * 取页面内容，请注意在结尾处调用
     */
    protected function _getContents() {
        return CLI_MODE ? false : ob_get_contents();
    }
    
    /**
     * 输出页面,并按需要做缓存
     */
    protected function _display($strPath, $arrAssign = null) {
        $__str_www_awawaya_com__ = TPLS_PATH . '/' . $strPath; // 这个变量名好怪啊,尽可能避免重复!
        if (file_exists($__str_www_awawaya_com__)) {
            if (!$this->_blnUsedCache) {
                is_array($arrAssign) && count($arrAssign) > 0 && extract($arrAssign);
                include($__str_www_awawaya_com__);
            }
        } else {
            $this->_cache_seconds = 0;
            $this->_blnPrint = true;
            echo 'Template error : ' . $__str_www_awawaya_com__;
        }
    }
}